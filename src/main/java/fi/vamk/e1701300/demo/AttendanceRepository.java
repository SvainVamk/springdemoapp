package fi.vamk.e1701300.demo;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {

    Attendance findByKey(String key);

    List<Attendance> findAllByDate(Date date);
}
